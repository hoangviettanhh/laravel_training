<?php
namespace App\Helper;

use Illuminate\Support\Str;

Trait Token
{
    public function SaveApiAuthToken()
    {
        $token = Str::random(60);
        $this->accept_token = $token;
        $this->save();
        return $this;
    }
}

