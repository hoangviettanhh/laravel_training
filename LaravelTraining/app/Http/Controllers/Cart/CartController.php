<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Http\Request\Cart\CartDeleteRequest;
use App\Http\Request\Cart\CartRequest;
use App\Http\Request\Cart\ListCartRequest;
use App\Repositories\CartRepository;

class CartController extends Controller
{
    protected $cartRepository;
   public function __construct(CartRepository $cartRepository)
   {
       $this->cartRepository = $cartRepository;
   }
   public function listCartByUser(ListCartRequest $req)
   {
       $this->cartRepository->ListCartByUser($req);
   }
   public function UpdateCreateCart(CartRequest $req)
   {
        $this->cartRepository->UpdateCreateCart($req);
   }
   public function DeleteCart(CartDeleteRequest $req)
   {
       $this->cartRepository->deleteCart($req);
   }
}
