<?php

namespace App\Http\Controllers\Order;

use App\Http\Controllers\Controller;
use App\Http\Request\Order\OrderDeleteRequest;
use App\Http\Request\Order\OrderRequest;
use App\Models\cart\CartDetail;
use App\Models\order\Order;
use App\Repositories\OrderRepository;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;


class OrderController extends Controller
{
    protected $orderRepository;

    public function __construct(OrderRepository $orderRepository)
    {
        $this->orderRepository =$orderRepository;
    }
    public function addOrderByUser(OrderRequest $req)
    {
        $this->orderRepository->addOrderByUser($req);
    }
    public function listOrderByUser($userId,$idOrder)
    {
        $this->orderRepository->listOrderByUser($userId,$idOrder);
    }
    public function deleteOrder(OrderDeleteRequest $req)
    {
        $this->orderRepository->deleteOrder($req);
    }
}
