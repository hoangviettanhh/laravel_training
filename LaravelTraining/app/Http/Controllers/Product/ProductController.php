<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Http\Request\Product\ListProductRequest;
use App\Http\Request\Product\ProductDeleteRequest;
use App\Http\Request\Product\ProductRequest;
use App\Http\Request\Product\UpdateProductRequest;
use App\Repositories\ProductRepository;


class ProductController extends Controller
{
    protected $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository =$productRepository;
    }

    public function listProductByUser(ListProductRequest $req)
    {
        return $this->productRepository->listProductByUser($req);
    }

    public function addProduct(ProductRequest $req)
    {
        return $this->productRepository->addProduct($req);
    }
    public function updateProduct(UpdateProductRequest $req)
    {
        return $this->productRepository->updateProduct($req);
    }

    public function deleteProductByUser(ProductDeleteRequest $req)
    {
        return $this->productRepository->deleteProductByUser($req);
    }
}
