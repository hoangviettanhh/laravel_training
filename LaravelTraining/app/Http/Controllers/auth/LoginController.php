<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Http\Request\UserAuth\LoginRequest;
use App\Http\Request\UserAuth\RegisterRequest;
use App\Http\Request\UserAuth\UserChangePassRequest;
use App\Models\auth\ResetPassword;
use App\Models\auth\User;
use App\Notifications\ResetPasswordRequest;
use App\Repositories\LoginRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    protected $loginRepository;
    public function __construct(LoginRepository $loginRepository)
    {
        $this->loginRepository = $loginRepository;
    }
    public function register(RegisterRequest $req)
    {
       $data = new User;
       $data->name = $req->username;
       $data->email = $req->email;
       $data->password = bcrypt($req->password);
       $data->save();
       return response([
          'status' =>200,
           'message'=> Lang::get('login.accept_register'),
           'data'=>$data
       ]);
    }
    public function login(LoginRequest $req)
    {
        $username = $req->username;
        $password = $req->password;
        $user = User::where('name',$username)->first();
        if (!$user && !Hash::check($password,$user->password,[])){
            return response([
               'status'=>0,
               'message'=> Lang::get('login.error_login')
            ],500);
        }
        $token = $user->createToken('authToken')->plainTextToken;
        return response([
            'status'=>1,
            'accept_token'=>$token,
            'accept_type'=>'Bearer'
        ]);
    }
    public function logout()
    {
        auth()->user()->tokens()->delete();
        return response([
            'status'=>200,
           'message' => 'LogOut'
        ]);
    }
    public function changePassword(UserChangePassRequest $req)
    {
        $password = $req->password;
        $againPass = $req ->again_pass;
        $passwordNew = $req ->password_new;
        $user = User::where('id',$req->id)->first();
        $data = [
            'password'=>bcrypt($passwordNew),
            'updated_at' => date('Y-m-d H:i:s', time()),
        ];
        if ($user && !Hash::check($password,$user->password) && $passwordNew === $againPass)
        {
                $user->update($data);
                return response([
                    'status'=>1,
                    'message'=>Lang::get('login.success_changePass'),
                    'data'=>$user
                ]);
        }
        return response([
            'status'=>0,
            'message'=>Lang::get('login.error_changePass'),
        ]);
    }
    public function sendMail(Request $req)
    {
        $email = $req->email;
        $user = User::where('email',$email)->firstOrFail();
        $passwordResets = ResetPassword::updateOrCreate(
            ['email'=>$user->email],
            ['token'=>Str::random(60)]);
        if ($passwordResets)
        {
            $user->notify(new ResetPasswordRequest($passwordResets->token));
        }
        return response([
            'status'=>1,
            'message'=>Lang::get('login.sendMail')
        ]);
    }
    public function resetPassword(Request $req,$token)
    {
        $passwordReset = ResetPassword::where('token',$token)->firstOrFail();
        if (Carbon::parse($passwordReset->update_at)->addMinutes(720)->isPast())
        {
            $passwordReset->delete();
            return response([
                'message'=>Lang::get('login.error_resetPass')
            ],422);
        }
        $user = User::where('email',$passwordReset->email)->firstOrFail();
        $updatePassword = $user->update($req->only('password'));
        $passwordReset->delete();
        return response([
            'status'=>1,
            'message'=>Lang::get('login.accept_resetPass'),
            'data'=>$updatePassword
        ]);
    }
}
