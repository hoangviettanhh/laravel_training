<?php

namespace App\Http\Controllers\auth;

use App\Http\Controllers\Controller;
use App\Http\Request\UserAuth\UserCreatedRequest;
use App\Http\Request\UserAuth\UserDeleteRequest;
use App\Http\Request\UserAuth\UserDetailProductRequest;
use App\Http\Request\UserAuth\UserUpdateRequest;
use App\Repositories\UserRepositories;

class UserController extends Controller
{
    protected $userRepository;
    public function __construct(UserRepositories $userRepositories)
    {
        $this->userRepository = $userRepositories;
    }
    public function listUser()
    {
        $this->userRepository->listUser();
    }
    public function createUser(UserCreatedRequest $req)
    {
        $this->userRepository->createUser($req);
    }
    public function updateUser(UserUpdateRequest $req)
        {
           $this->userRepository->updateUser($req);
        }
    public function listProductByUser($user)
    {
        $this->userRepository->listProductByUser($user);
    }
    public function detailProductByUser(UserDetailProductRequest $req)
    {
        $this->userRepository->detailProductByUser($req);
    }
    public function deleteUser(UserDeleteRequest $req)
    {
        $this->userRepository->deleteUser($req);
    }
}
