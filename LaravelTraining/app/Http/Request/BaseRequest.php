<?php

namespace App\Http\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;


class BaseRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            //
        ];
    }

    public function messages()
    {
        return [];
    }

    public function attributes()
    {
        return [];
    }
    protected function failedValidation(Validator $validator)
    {
        $errors = $validator->errors()->getMessages();
        $obj = $validator->failed();
        foreach ($obj as $input => $rules) {
            throw new HttpResponseException(
                response()->json([
                    'code' => 200,
                    'status' => 0,
                    'message' => $errors[$input][0]
                ], 200)
            );
        }
    }
}
