<?php
namespace App\Http\Request\Cart;

use App\Http\Request\BaseRequest;

class CartDeleteRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|int',
            'cart_id'=>'required|numeric|integer',
        ];
    }
}
