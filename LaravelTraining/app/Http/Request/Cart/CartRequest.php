<?php
namespace App\Http\Request\Cart;

use App\Http\Request\BaseRequest;

class CartRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'=>'required|numeric|integer',
            'id_product'=>'required|numeric|integer',
            'cart_amount'=>'required|string|numeric|min:1'
        ];
    }
}
