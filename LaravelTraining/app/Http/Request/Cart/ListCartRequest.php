<?php
namespace App\Http\Request\Cart;

use App\Http\Request\BaseRequest;

class ListCartRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
//        user_id
        return [
            'id'=>'required|numeric|integer',
        ];
    }
}
