<?php
namespace App\Http\Request\Order;

use App\Http\Request\BaseRequest;

class ListOrderRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required|numeric|int',
            'id_order' => 'required|numeric|int',
        ];
    }
}
