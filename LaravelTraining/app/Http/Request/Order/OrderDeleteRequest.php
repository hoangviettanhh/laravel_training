<?php
namespace App\Http\Request\Order;

use App\Http\Request\BaseRequest;

class OrderDeleteRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|int',
            'order_id'=>'required|numeric|integer',
        ];
    }
}
