<?php
namespace App\Http\Request\Order;

use App\Http\Request\BaseRequest;

class OrderRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
//        id user và id product
        return [
            'id'=>'required|numeric|integer',
            'product'=>'required',
        ];
    }
}
