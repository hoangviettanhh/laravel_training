<?php
namespace App\Http\Request\Product;

use App\Http\Request\BaseRequest;

class ListProductRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id'=>'required|numeric|integer',
        ];
    }
}
