<?php
namespace App\Http\Request\Product;

use App\Http\Request\BaseRequest;

class ProductDeleteRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
//            id user và id sản phẩm
            'id' => 'required|numeric|int',
            'pro_id'=>'required|numeric|integer',
        ];
    }
}
