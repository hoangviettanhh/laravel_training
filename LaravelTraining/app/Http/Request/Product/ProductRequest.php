<?php
namespace App\Http\Request\Product;

use App\Http\Request\BaseRequest;

class ProductRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'=>'required|numeric|integer',
            'pro_name'=>'required|string',
            'pro_price'=>'required|numeric|min:0',
            'pro_classify'=>'required|string',
        ];
    }
}
