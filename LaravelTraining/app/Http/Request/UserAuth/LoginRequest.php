<?php
namespace App\Http\Request\UserAuth;

use App\Http\Request\BaseRequest;

class LoginRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username'=>'required|string',
            'password'=>'required|string',
        ];
    }
}
