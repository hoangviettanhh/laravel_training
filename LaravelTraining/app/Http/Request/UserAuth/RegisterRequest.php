<?php
namespace App\Http\Request\UserAuth;

use App\Http\Request\BaseRequest;

class RegisterRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'username'=>'required|string|unique:tbtt_user',
            'email'=>'required|email|unique:tbtt_user',
            'password'=>'required|string',
        ];
    }
}
