<?php
namespace App\Http\Request\UserAuth;

use App\Http\Request\BaseRequest;

class UserChangePassRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|numeric|integer',
            "password" => 'required|string',
            "password_new" => 'required|string',
        ];
    }
}
