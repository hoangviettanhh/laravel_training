<?php
namespace App\Http\Request\UserAuth;

use App\Http\Request\BaseRequest;

class UserDeleteRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id'=>'required|numeric|integer',
        ];
    }
}
