<?php
namespace App\Http\Request\UserAuth;

use App\Http\Request\BaseRequest;

class UserDetailProductRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'user_id' => 'required|int',
        ];
    }
}
