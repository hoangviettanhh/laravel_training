<?php
namespace App\Http\Request\UserAuth;

use App\Http\Request\BaseRequest;

class UserUpdateRequest extends BaseRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'id' => 'required|int',
            "name" => 'unique:tbtt_user_laravel|required|string',
            "email" => 'unique:tbtt_user_laravel|required|email',
            "password" => 'required|string',
        ];
    }
}
