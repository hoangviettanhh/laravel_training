<?php

namespace App\Models\auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ResetPassword extends Model
{
   protected $table = 'password_resets';
   protected $fillable = [
     'email',
     'token'
   ];
}
