<?php

namespace App\Models\auth;

use App\Helper\Token;
use App\Models\order\Order;
use App\Models\product\product;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Lang;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens,HasFactory,Notifiable,Token;
    protected $table = 'tbtt_user_laravel';
    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'role'
    ];
    protected $hidden = [
      'password',
      'remember_token'
    ];
    public function product()
    {
        return $this->hasMany(product::class,'pro_user');
    }
    public function order()
    {
        return $this->hasMany(Order::class,'order_user','id');
    }
    /*Danh sách sản phẩm của 1 user*/
    public function listProductByUser($user)
    {
        if (!empty($user))
        {
            $product = product::select('pro_user')
                ->groupby('pro_user')
                ->first();
            return $product;
        }
        return response([
           'status'=>0,
           'message'=>Lang::get('user.error_user')
        ]);
    }
    /*Phân trang theo user*/
    public function paginateUserProduct($user)
    {
        if (!empty($user))
        {
        $userPaginate=User::select('user_id','name')
            ->whereIn('user_id',$this->listProductByUser($user))
            ->paginate(2);
        return $userPaginate;
        }
        return response([
            'status'=>0,
            'message'=>Lang::get('user.error_user')
        ]);
    }
    /*Chi tiết sản phẩm của 1 user*/
    public function listDetailProductByUser($user)
    {
        if (!empty($user))
        {
        $user = User::select('id','name','email','password')
            ->where('id',$user)
            ->with(['product'=>function($query){
                $query->select('pro_name','pro_user','pro_price');
            }])
            ->first();
        return $user;
        }
        return response([
            'status'=>0,
            'message'=>Lang::get('user.error_user')
        ]);
    }
}
