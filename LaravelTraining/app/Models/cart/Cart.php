<?php

namespace App\Models\cart;

use App\Models\product\product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'tbtt_cart';

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'cart_product',
        'cart_user',
    ];
    public function product()
    {
        return $this->hasMany(product::class,'id','cart_product');
    }
    public function cartdetail()
    {
        return $this->hasOne(CartDetail::class,'cart_id','id');
    }
    /*Danh sách giỏ hàng của 1 user*/
    public function listCartByUser($user)
    {
        $listCartByUser = Cart::select( 'tbtt_product_laravel.id','cart_user','cart_amount',
            'tbtt_product_laravel.pro_name','tbtt_product_laravel.pro_price')
            ->leftjoin('tbtt_product_laravel','tbtt_cart.cart_product','=','tbtt_product_laravel.id')
            ->where('cart_user',$user)
            ->get();
        return $listCartByUser;
    }
}
