<?php

namespace App\Models\cart;

use App\Models\product\product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
    protected $table = 'tbtt_cart_detail';

    protected $primaryKey = 'detail_id';
    public $timestamps = false;
    protected $fillable = [
        'cart_id',
        'cart_name',
        'cart_price',
        'cart_amount',
        ];
    public function cart()
    {
        return $this->belongsTo(Cart::class,'id','cart_id');
    }
}
