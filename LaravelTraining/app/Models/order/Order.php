<?php

namespace App\Models\order;

use App\Models\cart\Cart;
use App\Models\cart\CartDetail;
use App\Models\product\product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'tbtt_order';

    protected $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'id',
        'order_user',
        'product'
    ];
    public function detail()
    {
        return $this->hasMany(OrderDetail::class,'order_id','id');
    }
    public function product()
    {
        return $this->hasMany(product::class,'pro_user','order_user');
    }
    public function cart()
    {
        return $this->hasMany(Cart::class,'cart_product','product');
    }
}
