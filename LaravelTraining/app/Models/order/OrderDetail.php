<?php

namespace App\Models\order;

use App\Models\product\product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;

class OrderDetail extends Model
{
    protected $table = 'tbtt_order_detail';

    protected $primaryKey = 'order_id';
    public $timestamps = false;
    protected $fillable = [
        'order_id',
        'pro_id',
        'detail_name',
        'detail_price',
        'detail_amount'
    ];
    public function product()
    {
        return $this->belongsTo(product::class,'pro_id','pro_id');
    }
    /*Danh sách hóa đơn của 1 user*/
    public function listOrderByUser($user,$idOrder)
    {
        if (!empty($user) && !empty($idOrder))
        {
        $order = Order::select('id','order_user')
            ->where('id',$idOrder)
            ->where('order_user',$user)
            ->with(['detail'=>function($query){
                $query->select('order_id','pro_id','detail_name');
            }])
            ->first();
        return $order;
        }
        return response([
            'status' => 0,
            'message' => Lang::get('order_error_user')
        ]);
    }
}
