<?php

namespace App\Models\product;

use App\Models\auth\User;
use App\Models\cart\Cart;
use App\Models\order\OrderDetail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class product extends Model
{
    protected $table = 'tbtt_product_laravel';
    public $timestamps = false;
    protected $primaryKey = 'id';
    protected $fillable = [
        'id',
        'pro_user',
        'pro_name',
        'pro_detail',
        'pro_price',
        'pro_classify'
    ];
    public function detail()
    {
        return $this->hasOne(OrderDetail::class,'pro_id','id');
    }

    /*Danh sách sản phẩm của 1 user*/
    public function listProductByUser($user)
    {
        $product = product::select('id', 'pro_user', 'pro_name', 'pro_price',
            'pro_classify')
            ->where('pro_user', $user)
            ->get();
        return $product;
    }
}
