<?php
namespace App\Repositories;

use App\Models\cart\Cart;
use App\Models\product\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;


class CartRepository
{
    public function ListCartByUser($user)
    {
        $cart = new Cart;
        $listCartByUser = $cart->listCartByUser($user);
        return response([
            'status'=>1,
            'message'=>Lang::get('cart.listCart'),
            'data'=>$listCartByUser,
        ]);
    }
    public function UpdateCreateCart(Request $req)
    {
        $data = [
            'cart_amount'=>$req->cart_amount,
            'cart_user'=>$req->user_id
        ];
        $updateOrcreate =  Cart::updateOrCreate(
            [
                'cart_product' =>$req->id_product,
                'cart_user'=>$req->user_id
            ], $data);
            return response([
                'status' =>1,
                'message'=>Lang::get('cart.success_cart'),
                'data'=>$updateOrcreate
            ]);
    }
    public function deleteCart(Request $req)
    {
        $cart = Cart::where('id',$req->cart_id)
            ->where('cart_user',$req->id)
            ->first();
        if ($cart)
        {
            $cart->delete();
            return response([
                'status'=>1,
                'message'=>Lang::get('cart.success_delete'),
                'data'=>$cart
            ]);
        }
        return response([
            'status'=>0,
            'message'=>Lang::get('cart.error_delete')
        ]);
    }
}
