<?php
namespace App\Repositories;

use App\Models\auth\User;
use App\Models\cart\Cart;
use App\Models\product\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;


class LoginRepository
{
    public function login(Request $req)
    {
        $data = [
            'name' => $req->username,
            'password' =>$req->password,
        ];
        if (Auth::guard('web')->attempt($data)) {
            $user = Auth::guard('web')
                ->user()
                ->SaveApiAuthToken();

            return response([
                'status' =>1,
                'message'=>Lang::get('login.success_login'),
                'data'=>$user
            ]);
        }

        return response()->json(
            [   'status'=>401,
                'message' => Lang::get('login.error_login')]);
    }
}
