<?php
namespace App\Repositories;

use App\Models\cart\Cart;
use App\Models\cart\CartDetail;
use App\Models\order\Order;
use App\Models\order\OrderDetail;
use App\Models\product\product;
use App\Models\auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Lang;


class OrderRepository
{
    public function listOrderByUser($user,$idOrder)
    {
        $orderDetail = new OrderDetail;
        $listOrderByUser = $orderDetail->listOrderByUser($user,$idOrder);
            return response([
                'status' => 1,
                'message' => Lang::get('order.listCart'),
                'data' => $listOrderByUser
            ]);
    }
    public function addOrderByUser(Request $req)
    {
        $order = [
            'order_user'=>$req->id,
            'updated_at' => date('Y-m-d H:i:s', time()),
            'created_at' => date('Y-m-d H:i:s', time()),
        ];
        $addOrder = DB::table('tbtt_order')
            ->insertGetId($order);
        $product = $req->product;

        foreach ($product as $value)
        {
            $orderDetail = CartDetail::select('cart_id','cart_user','cart_price','tbtt_cart.id','tbtt_product_laravel.pro_name',
                'tbtt_cart.cart_amount','tbtt_product_laravel.id','tbtt_cart.cart_product')
                ->leftjoin('tbtt_cart','tbtt_cart.id','=','tbtt_cart_detail.cart_id')
                ->leftjoin('tbtt_product_laravel','tbtt_product_laravel.id','=','tbtt_cart.cart_product')
                ->where('cart_product',$value['pro_id'])
                ->where('cart_user',$req->id)
                ->first();
            if ($orderDetail)
            {
                $addOrderDetail = [
                    'order_id'=>$addOrder,
                    'pro_id'=>$value['pro_id'],
                    'detail_name'=>$orderDetail->pro_name,
                    'detail_price'=>$orderDetail->cart_price,
                    'detail_amount'=>$orderDetail->cart_amount,
                    'updated_at' => date('Y-m-d H:i:s', time()),
                    'created_at' => date('Y-m-d H:i:s', time()),
                ];
                $dataOrder = OrderDetail::create($addOrderDetail);
            }
        }
        return response([
            'status'=>1,
            'message'=>Lang::get('order.success_order'),
            'data'=>$dataOrder
        ]);
    }
    public function deleteOrder(Request $req)
    {
        $order = Order::where('id',$req->order_id)
            ->where('order_user',$req->id)
            ->first();
        if ($order)
        {
            $order->delete();
            return response([
                'status'=>1,
                'message'=>Lang::get('order.success_delete'),
                'data'=>$order
            ]);
        }
        return response([
            'status'=>0,
            'message'=>Lang::get('order.error_delete')
        ]);
    }
}
