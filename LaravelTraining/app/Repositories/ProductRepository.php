<?php
namespace App\Repositories;

use App\Models\product\product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;


class ProductRepository
{
    public function listProductByUser($user)
    {
        $product = new product;
        $proByUser =$product->listProductByUser($user);
        return response([
            'status' => 1,
            'message' => Lang::get('product.listProduct'),
            'data' => $proByUser
        ]);
    }
    public function addProduct(Request $req)
    {
        $user = $req->user_id;
            $product = [
                'pro_user' => $user,
                'pro_name' => $req->pro_name,
                'pro_price' => $req->pro_price,
                'pro_classify' => $req->pro_classify,
                'created_at' => date('Y-m-d H:i:s', time()),
            ];

            if (product::create($product)) {
                return response([
                    'status' => 1,
                    'message' => Lang::get('product.success_product'),
                    'data' => $product
                ]);
            }
            return response([
                'status' => 0,
                'message' => Lang::get('product.error_product'),
            ]);

    }

    public function updateProduct(Request $req)
    {
        $user = $req->user_id;
        $product = $req->id_product;
                $productUpdate = [
                    'pro_name' => $req->pro_name,
                    'pro_price' => $req->pro_price,
                    'pro_classify' => $req->pro_classify,
                    'updated_at' => date('Y-m-d H:i:s', time()),
                ];
                if (product::where('id',$product)
                    ->where('pro_user',$user)
                    ->update($productUpdate)) {
                    return response([
                        'status' => 1,
                        'message' => Lang::get('product.success_update'),
                        'data' => $productUpdate
                    ]);
                }
                return response([
                    'status'=>0,
                    'message'=> Lang::get('product.error_update')
                ]);
    }
    public function deleteProductByUser(Request $req)
    {
        $product = product::where('id',$req->pro_id)
                    ->where('pro_user',$req->id)
                    ->first();
        if ($product)
        {
            $product->delete();
            return response([
                'status'=>1,
                'message'=>Lang::get('order.success_delete'),
                'data'=>$product
            ]);
        }
        return response([
            'status'=>0,
            'message'=>Lang::get('order.error_delete')
        ]);
    }
}
