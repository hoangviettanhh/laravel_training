<?php
namespace App\Repositories;

use App\Models\auth\User;
use App\Models\product\product;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class UserRepositories
{
    public function listUser()
    {
        if (Auth::user()->role === 'Admin') {
            $user = User::select('id', 'name','role', 'email', 'password',
                'created_at', 'updated_at')
                ->get();
            return response([
                'status' => 1,
                'message' => Lang::get('user.success_listUser'),
                'data' => $user
            ]);
        }
        return response([
            'status' => 0,
            'message' => Lang::get('user.error_role'),
        ]);
    }
    public function createUser(Request $req)
    {
        if (Auth::user()->role === 'Admin')
        {
            $created = [
                'user_id' => $req->user_id,
                'name' => $req->name,
                'email' => $req->email,
                'password' => $req->password,
                'updated_at' => date('Y-m-d H:i:s', time()),
                'created_at' => date('Y-m-d H:i:s', time()),
            ];
            if (User::create($created)) {
                return response([
                    'status' => 1,
                    'message' => Lang::get('user.success_addUser'),
                    'data' => $created
                ]);
            }
            return response([
                'status' => 0,
                'message' => Lang::get('user.error_addUser')
            ]);
        }
        return response([
            'status' => 0,
            'message' => Lang::get('user.error_role'),
        ]);
    }
    public function updateUser(Request $req)
    {
        if (Auth::user()->role === 'Admin')
        {
            $updateorcreate = [
                'name' => $req->name,
                'email' => $req->email,
                'password' => $req->password,
                'updated_at' => date('Y-m-d H:i:s', time()),
            ];
            if (User::where('user_id',$req->user_id)
                ->update($updateorcreate)) {
                return response([
                    'status' => 1,
                    'message' => Lang::get('user.success_updateUser'),
                    'data' => $updateorcreate
                ]);
            }
            return response([
                'status' => 0,
                'message' => Lang::get('user.error_updateUser')
            ]);
        }
        return response([
            'status' => 0,
            'message' => Lang::get('user.error_role'),
        ]);
    }
    public function listProductByUser($userId)
    {
        $user = new User;
        $productByUser = $user->listProductByUser($userId);
        $paginate = $user->paginateUserProduct($userId);
        return response([
            'status' => 1,
            'message' => Lang::get('user.success_userByProduct'),
            'data' => $paginate,$productByUser
        ]);
    }
    public function detailProductByUser($userId)
    {
        $user = new User;
        $listDetailByUser = $user->listDetailProductByUser($userId);
        return response([
            'status' => 1,
            'message' => Lang::get('user.success_detailProductByUser'),
            'data' => $listDetailByUser
        ]);
    }
    public function deleteUser(Request $req)
    {
        $user = User::where('id', $req->user_id)->first();
        if (Auth::user()->role === 'Admin')
        {
            if ($user) {
                $user->delete();
                return response([
                    'status' => 1,
                    'message' => Lang::get('user.success_deleteUser'),
                    'data' => $user
                ]);
            }
            return response([
                'status' => 0,
                'message' => Lang::get('user.error_deleteUser')
            ]);
        }
        return response([
            'status' => 0,
            'message' => Lang::get('user.error_role'),
        ]);
    }
}
