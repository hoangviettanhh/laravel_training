<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbtt_product_laravel', function (Blueprint $table) {
            $table->id();
            $table->integer('pro_id')->nullable();
            $table->string('pro_name')->nullable();
            $table->string('pro_detail')->nullable();
            $table->string('pro_price')->nullable();
            $table->string('pro_classify')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbtt_product_laravel');
    }
};
