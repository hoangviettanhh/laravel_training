<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbtt_order_detail', function (Blueprint $table) {
            $table->id();
            $table->integer('detail_id')->nullable();
            $table->integer('detail_order')->nullable();
            $table->string('detail_name')->nullable();
            $table->string('detail_price')->nullable();
            $table->string('detail_note')->nullable();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbtt_order_detail');
    }
};
