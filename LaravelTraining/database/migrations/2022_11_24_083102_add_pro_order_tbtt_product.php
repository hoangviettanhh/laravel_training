<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbtt_product_laravel', function (Blueprint $table) {
            $table->integer('pro_order')->after('pro_user')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbtt_product_laravel', function (Blueprint $table) {
            $table->dropColumn('pro_order');
        });
    }
};
