<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbtt_cart', function (Blueprint $table) {
            $table->id();
            $table->integer('cart_product')->nullable();
            $table->integer('cart_user')->nullable();
            $table->string('amount')->nullable();
            $table->boolean('cart_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbtt_cart');
    }
};
