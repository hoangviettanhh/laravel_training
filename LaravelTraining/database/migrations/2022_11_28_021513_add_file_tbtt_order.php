<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbtt_order', function (Blueprint $table) {
            $table->integer('order_product')->after('order_user')->nullable();
            $table->string('order_name')->after('order_product')->nullable();
            $table->string('order_price')->after('order_name')->nullable();
            $table->string('order_detail')->after('order_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbtt_order', function (Blueprint $table) {
            $table->dropColumn('order_product');
            $table->dropColumn('order_name');
            $table->dropColumn('order_price');
            $table->dropColumn('order_detail');
        });
    }
};
