<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbtt_order_detail', function (Blueprint $table) {
            $table->integer('pro_id')->after('order_id')->nullable();
            $table->string('detail_name')->after('pro_id')->nullable();
            $table->string('detail_price')->after('detail_name')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbtt_order_detail', function (Blueprint $table) {
            $table->dropColumn('pro_id');
            $table->dropColumn('detail_name');
            $table->dropColumn('detail_price');
        });
    }
};
