<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbtt_cart_detail', function (Blueprint $table) {
            $table->string('cart_name')->after('cart_id')->nullable();
            $table->string('cart_price')->after('cart_name')->nullable();
            $table->string('cart_amount')->after('cart_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbtt_cart_detail', function (Blueprint $table) {
            $table->dropColumn('cart_id');
            $table->dropColumn('cart_name');
            $table->dropColumn('cart_price');
            $table->dropColumn('cart_amount');
        });
    }
};
