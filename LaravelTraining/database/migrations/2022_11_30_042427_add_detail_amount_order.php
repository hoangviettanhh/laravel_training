<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbtt_order_detail', function (Blueprint $table) {
            $table->string('detail_amount')->after('detail_price')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tbtt_order_detail', function (Blueprint $table) {
            $table->dropColumn('detail_amount');
        });
    }
};
