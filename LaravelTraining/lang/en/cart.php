<?php

return [
    'success_cart' => 'Thêm sản phẩm vào giỏ hàng thành công',
    'success_update' => 'Chỉnh sửa sản phẩm thành công',
    'success_delete' => 'Xóa sản phẩm thành công ',
    'error_delete' => 'Xóa sản phẩm thành công thất bại ! Vui lòng thử lại  ',
    'error_amount' => 'Số lượng sản phẩm không hợp lệ ! Vui lòng thử lại',
    'error_cart' => 'Sản phẩm không hợp lệ hoặc không tồn tại ! Vui lòng thử lại ',
    'listCart' => 'Các sản phẩm trong giỏ hàng của bạn',
];
