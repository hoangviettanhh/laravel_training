<?php

return [
    'success_login' => 'Đăng nhập thành công',
    'success_changePass' => 'Thay đổi Password thành công',
    'accept_login' => 'Vui lòng đăng nhập !',
    'accept_register' => 'Đăng ký thành công ',
    'accept_resetPass' => 'Quên mật khẩu thành công ',
    'error_login' => 'Sai mật khẩu hoặc tên đăng nhập ! Vui lòng thử lại',
    'error_user' => 'Người dùng không tồn tại !',
    'error_changePass' => 'Thay đổi mật khẩu thất bại ! Vui lòng thử lại',
    'error_againPass' => 'Xác thực mật khẩu mới sai ! Vui lòng thử lại',
    'error_resetPass' => 'Mã xác thực không hợp lệ ! Vui lòng thử lại',
    'sendMail' => 'Gửi Mail xác nhận thành công',
];
