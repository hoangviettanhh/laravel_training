<?php

return [
    'success_order' => 'Thêm đơn hàng thành công',
    'success_delete' => 'Xóa đơn hàng thành công',
    'error_order' => 'Tạo đơn hàng thất bại !',
    'error_delete' => 'Xóa đơn hàng thất bại ! Vui lòng thử lại',
    'listCart' => 'Danh sách đơn hàng của bạn',
    'error_user' => 'Người dùng không hợp lệ',
];
