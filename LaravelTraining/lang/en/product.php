<?php

return [
    'success_product' => 'Thêm sản phẩm thành công',
    'success_update' => 'Chỉnh sửa sản phẩm thành công',
    'delete_success' => 'Xóa sản phẩm thành công',
    'delete_error' => 'Xóa sản phẩm thất bại ! Vui lòng thử lại',
    'error_product' => 'Thêm sản phẩm thất bại ! Vui lòng thử lại',
    'error_update' => 'Chỉnh sửa thất bại ! Vui lòng thử lại',
    'error_user' => 'Người dùng không tồn tại ! Vui lòng thử lại',
    'listProduct'=>'Danh sách sản phẩm của bạn'
];
