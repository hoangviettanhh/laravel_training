<?php

return [
    'success_listUser' => 'Danh sách người dùng',
    'success_deleteUser' => 'Xóa người dùng thành công',
    'success_userByProduct' => 'Danh sách sản phẩm người dùng',
    'success_detailProductByUser' => 'Chi tiết sản phẩm của người dùng',
    'success_addUser' => 'Thêm mới thành công!',
    'success_updateUser' => 'Chỉnh sửa thành công!',
    'error_addUser' => 'Thêm mới thất bại',
    'error_deleteUser' => 'Xóa người dùng thất bại ! Vui lòng kiểm tra lại',
    'error_updateUser' => 'Chỉnh sửa thất bại',
    'error_login' => 'Sai mật khẩu hoặc tên đăng nhập ! Vui lòng thử lại',
    'error_role' => 'Bạn không có quyền !',
    'error_user' => 'Người dùng không hợp lệ!',
];
