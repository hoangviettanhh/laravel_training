<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\UserController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\product\ProductController;
use App\Http\Controllers\order\OrderController;
use App\Http\Controllers\cart\CartController;
use App\Http\Middleware\Authenticate;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('login',[LoginController::class,'login'])->name('login');
Route::post('register',[LoginController::class,'register']);
Route::post('logout',[LoginController::class,'logout']);
Route::post('changePassword',[LoginController::class,'changePassword']);
Route::post('resetPassword',[LoginController::class,'sendMail']);
Route::put('resetPassword/{token}',[LoginController::class,'resetPassword']);
Route::group(['prefix' => 'user', 'namespace' => 'auth','middleware'=>'auth:sanctum'], function () {
    Route::get('listUser', [UserController::class,'listUser']);
    Route::get('userByProduct/{user}', [UserController::class,'listProductByUser']);
    Route::get('detailProductByUser', [UserController::class,'detailProductByUser']);
    Route::post('createUser', [UserController::class,'createUser']);
    Route::post('updateUser', [UserController::class,'updateUser']);
    Route::post('deleteUser', [UserController::class,'deleteUser']);
});

Route::group(['prefix' => 'product', 'namespace' => 'Product','middleware'=>'auth:sanctum'], function () {
    Route::get('listProduct/{user}', [ProductController::class,'listProductByUser']);
    Route::post('addProduct', [ProductController::class,'addProduct']);
    Route::post('updateProduct', [ProductController::class,'updateProduct']);
    Route::post('deleteProduct', [ProductController::class,'deleteProductByUser']);
});
Route::group(['prefix' => 'cart', 'namespace' => 'Cart','middleware'=>'auth:sanctum'], function () {
    Route::get('listCart/{user}', [CartController::class,'listCartByUser']);
    Route::post('UpdateCreateCart', [CartController::class,'UpdateCreateCart']);
    Route::post('DeleteCart', [CartController::class,'DeleteCart']);
});

Route::group(['prefix' => 'order', 'namespace' => 'Order','middleware'=>'auth:sanctum'], function () {
    Route::get('listOrder/{userId}/{id}', [OrderController::class,'listOrderByUser']);
    Route::post('addOrder', [OrderController::class,'addOrderByUser']);
    Route::post('deleteOrder', [OrderController::class,'deleteOrder']);
});



